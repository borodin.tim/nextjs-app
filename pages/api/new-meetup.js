import { MongoClient } from 'mongodb';

const handler = async (req, res) => {
    if (req.method === 'POST') {
        const data = req.body;

        try {
            const client = await MongoClient.connect(process.env.MONGODB_URL);
            const db = client.db();
            const meetupsCollection = db.collection(process.env.MONGODB_COLLECTION_NAME);

            const result = await meetupsCollection.insertOne(data);

            client.close
            res.status(201).json({ message: 'Meetup inserted' });
        } catch (err) {
            console.log(err);
        }
    }
}

export default handler;