import { Fragment } from 'react';
import Head from 'next/head';
import { MongoClient, ObjectId } from 'mongodb';

import MeetupDetail from '../../components/meetups/MeetupDetail';

const MeetupDetailsPage = props => {
    return (
        <Fragment>
            <Head>
                <title>{props.meetupData.title}</title>
                <meta name="description" content={props.meetupData.description} />
            </Head>
            <MeetupDetail
                image={props.meetupData.image}
                title={props.meetupData.title}
                address={props.meetupData.address}
                description={props.meetupData.description}
            />
        </Fragment>
    );
};

export const getStaticPaths = async () => {
    const client = await MongoClient.connect(process.env.MONGODB_URL);

    const db = client.db();
    const meetupsCollection = db.collection(process.env.MONGODB_COLLECTION_NAME);

    const meetups = await meetupsCollection.find({}, { _id: true }).toArray();

    client.close();

    return {
        paths: meetups.map(meetup => ({ params: { meetupId: meetup._id.toString() } })),
        fallback: 'blocking'
    };
};

export const getStaticProps = async (context) => {
    const meetupId = context.params.meetupId;

    const client = await MongoClient.connect(process.env.MONGODB_URL);

    const db = client.db();
    const meetupsCollection = db.collection(process.env.MONGODB_COLLECTION_NAME);

    const meetup = await meetupsCollection.findOne({ _id: ObjectId(meetupId) });

    client.close();

    return {
        props: {
            meetupData: {
                id: meetup._id.toString(),
                title: meetup.title,
                address: meetup.address,
                image: meetup.image,
                description: meetup.description,
            }
        }

    };
};

export default MeetupDetailsPage;