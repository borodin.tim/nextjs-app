import Head from 'next/head';
import { MongoClient } from 'mongodb';

import MeetupList from '../components/meetups/MeetupList';
import { Fragment } from 'react';

const HomePage = props => {
    return (
        <Fragment>
            <Head>
                <title>Meetups - NextJS</title>
                <meta name="description" content="Browse a list of highly  active meetups on React-NextJS" />
            </Head>
            <MeetupList meetups={props.meetups} />
        </Fragment>
    );
};

// Runs once while build process while deploying.
// Faster than getServerSideProps
export const getStaticProps = async () => {
    const client = await MongoClient.connect(process.env.MONGODB_URL);

    const db = client.db();
    const meetupsCollection = db.collection(process.env.MONGODB_COLLECTION_NAME);

    const meetups = await meetupsCollection.find().toArray();

    client.close();

    return {
        props: {
            meetups: meetups.map(meetup => ({
                id: meetup._id.toString(),
                title: meetup.title,
                image: meetup.image
            }))
        },
        revalidate: 10
    };
};
// revalidate - Feature Incremental Static Generation.
// 'revalidate: 10' means re-pre-generate page on the server every 10 seconds

// Will always run on the server for every request
// More useful for requests like authentication...
// export const getServerSideProps = async (context) => {
//     const req = context.req;
//     const res = context.res;

//     // fetch data from API ...
//     return {
//         props: {
//             meetups: DUMMY_MEETUPS
//         }
//     };
// };

export default HomePage;

